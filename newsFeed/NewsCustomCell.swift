//
//  NewsCustomCell.swift
//  newsFeed
//
//  Created by Artem on 12/4/16.
//  Copyright © 2016 Artem Velykyy. All rights reserved.
//

import UIKit

class NewsCustomCell: UITableViewCell {
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var authorName: UILabel!
    @IBOutlet weak var articleDate: UILabel!
    @IBOutlet weak var articleImage: UIImageView!

}
