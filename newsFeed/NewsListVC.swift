//
//  ViewController.swift
//  newsFeed
//
//  Created by Artem on 12/2/16.
//  Copyright © 2016 Artem Velykyy. All rights reserved.
//

import UIKit
import Alamofire
import MediaPlayer

let apiKey = "344671f98e5646f8b29bdc7e7cbea248"
let source = "the-next-web"
let mP = MPMediaLibrary()

class NewsListVC: UIViewController {
    //outlets
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var loadingActivityInd: UIActivityIndicatorView!
    @IBOutlet weak var newsTableView: UITableView!
    
    //actions

    @IBAction func adAppear(_ sender: Any) {
        
    }
    
    var news = [NewsModel]()
    let sourceData = ["abc-news-au", "bbc-news", "cnn", "focus", "the-new-york-times", "time"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        makeRequest()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    
    func makeRequest() {
        loadingActivityInd.startAnimating()
        Alamofire.request("https://newsapi.org/v1/articles?source=\(source)&sortBy=latest&apiKey=\(apiKey)").responseJSON { response in
            
            if let json = response.result.value as? NSDictionary {
                
                
                self.news.removeAll()
                let newArticles = self.parseJSONObject(json: json["articles"] as! [[String: Any]])
                self.news = newArticles
                self.newsTableView.reloadData()
                self.loadingActivityInd.stopAnimating()
            }
        }
        
    }
    
    
    private func parseJSONObject(json objects: [[String: Any]]) -> [NewsModel] {
        var objectNews = [NewsModel]()
        for object in objects {
            objectNews.append(NewsModel(
                author: object["s"] as? String ?? "",
                title: object["title"] as? String ?? "",
                description: object["description"] as? String ?? "",
                url: object["url"] as? String ?? "",
                urlToImage: object["urlToImage"] as? String ?? "",
                publishedAt: object["publishedAt"] as? String ?? ""))
        }
        return objectNews
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetail" {
            if let indexPath = newsTableView.indexPath(for: sender as! UITableViewCell) {
                let destVC = segue.destination as! DetailNewsVC
                destVC.linkOfTheNews = news[indexPath.row].url
            }
        }
    }
    // MARK: Cell Animation
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

//        let degree: Double = 90
//        let rotationAngle = CGFloat(degree * M_PI / 180)
//        let rotationTransform = CATransform3DMakeRotation(rotationAngle, 0, 0, 0)
//        cell.layer.transform = rotationTransform
//        
//        UIView.animate(withDuration: 1, delay: 0.2 * Double(indexPath.row), options: .curveEaseInOut, animations: {
//            cell.layer.transform = CATransform3DIdentity
//        })
        let translationTrasform = CATransform3DTranslate(CATransform3DIdentity, 0, -500, 0)
        cell.layer.transform = translationTrasform
        
        UIView.animate(withDuration: 1, delay: 0.2 * Double(indexPath.row), options: .curveEaseInOut, animations: {
            cell.layer.transform = CATransform3DIdentity})

    }

    
}

extension NewsListVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return news.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = newsTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NewsCustomCell
        
        cell.articleDate.text = news[indexPath.row].publishedAt
        cell.articleDate.highlightedTextColor = UIColor.black
        cell.authorName.text = news[indexPath.row].author
        cell.authorName.highlightedTextColor = UIColor.black
        cell.newsTitle.text = news[indexPath.row].title
        cell.newsTitle.highlightedTextColor = UIColor.black
        
        //Image stack
        
        cell.articleImage.alpha = 0
        cell.articleImage.image = UIImage(named: "image_loading")
        ImageDownloader().downloadImages(news[indexPath.row].urlToImage, ifPhotoAvailable: { (image) in
            UIView.animate(withDuration: 0.3, animations: {
                cell.articleImage.image = image
                cell.articleImage.alpha = 1
            })
        }, ifPhotoUnavailable:  { (image) in
            UIView.animate(withDuration: 0.3, animations: {
                cell.articleImage.image = image
                cell.articleImage.alpha = 1
            })
            
        })
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        newsTableView.deselectRow(at: indexPath, animated: true)
    }
    
}


