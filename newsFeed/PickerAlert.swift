//
//  PickerAlert.swift
//  NotaryService
//
//  Created by Artem on 10/4/16.
//  Copyright © 2016 Artem Velykyy. All rights reserved.
//

import UIKit

class PickerAlert: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    
    
    var window: UIWindow?
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //Outlets
    
    @IBOutlet weak var viewToShow: UIView!
    @IBOutlet weak var picker: UIPickerView!
    
    
    var superClass = AdvanceSVC()
    
    var pickerData: [String]!
    var choosenComponent = String()
    var numberOfTheArray = Int()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewToShow.transform = CGAffineTransform(scaleX: 0, y: 0)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        choosenComponent = pickerData[0]
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
     return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func showInView(_ parentView:UIView, animated: Bool = true) {
        parentView.addSubview(self.view)
        if animated {
            self.showAnimated()
        }
    }
    
    func showAnimated() {
        UIView.animate(withDuration: 0.3) { 
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            self.viewToShow.transform = CGAffineTransform.identity
            
        }
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        choosenComponent = pickerData[row]
    }
    
    @IBAction func chosen(_ sender: UIButton) {
        switch numberOfTheArray {
        case 1: superClass.pickedArea = choosenComponent
        case 2: superClass.pickedCity = choosenComponent
        case 3: superClass.pickedDist = choosenComponent
        case 4: superClass.pickedStr = choosenComponent
        default: return
        }
        self.view.removeFromSuperview()
    }
    
    
    

}
