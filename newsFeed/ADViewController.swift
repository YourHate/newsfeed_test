//
//  ADViewController.swift
//  newsFeed
//
//  Created by Artem on 5/11/17.
//  Copyright © 2017 Artem Velykyy. All rights reserved.
//

import UIKit

class ADViewController: UIViewController, RevMobAdsDelegate
{
    
    @IBOutlet weak var tableView: UITableView!

    var bannerView: RevMobBannerView?
    var banner: RevMobBanner?
    var defaults = UserDefaults.standard
    let constant = Constant()
    var listArray = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let qqNib = UINib(nibName: "qqTableViewCell", bundle: nil)
        let simpleNib = UINib(nibName: "mainTableViewCell", bundle: nil)
        //tableView.register(nib, forCellReuseIdentifier: "cell")
        self.tableView.register(qqNib, forCellReuseIdentifier: "celler")
        self.tableView.register(simpleNib, forCellReuseIdentifier: "cell")
        //tableView.reloadData()
        }
        
}


extension ADViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let identifier = "cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? mainTableViewCell
        cell?.tableSetup(id: indexPath.row)

        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
}
