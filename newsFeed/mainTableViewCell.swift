//
//  mainTableViewCell.swift
//  newsFeed
//
//  Created by Artem on 5/12/17.
//  Copyright © 2017 Artem Velykyy. All rights reserved.
//

import UIKit

class mainTableViewCell: UITableViewCell {

    var appID: String = ""
    let constant = Constant()
    
    @IBOutlet weak var bannerView: RevMobBannerView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func tableSetup(id: Int) {
        if id % 5 == 0 {
        appID = Constant.bannersIds[id/5]
        print(appID)
        let ad = Ad(appID: appID, adType: .Banner, bannerView: bannerView, minAge: 10)
            ad.startSession()
        }
    }
    
    
}
