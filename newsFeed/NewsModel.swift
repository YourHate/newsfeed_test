//
//  news.swift
//  newsFeed
//
//  Created by Artem on 12/2/16.
//  Copyright © 2016 Artem Velykyy. All rights reserved.
//

import Foundation


struct NewsModel {
    
    var author: String
    var title: String
    var description: String
    var url: String
    var urlToImage: String
    var publishedAt: String
    
}
