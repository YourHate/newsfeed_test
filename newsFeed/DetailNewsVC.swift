//
//  DetailNewsVC.swift
//  newsFeed
//
//  Created by Artem on 12/4/16.
//  Copyright © 2016 Artem Velykyy. All rights reserved.
//

import UIKit
import WebKit

class DetailNewsVC: UIViewController {
    
    @IBOutlet weak var loadingActivityInd: UIActivityIndicatorView!
    
    var linkOfTheNews: String!
    
     var webView: WKWebView!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupWebView(link: linkOfTheNews)
    }
    
    
    
    private func setupWebView(link toShow: String) {
        loadingActivityInd.startAnimating()
        print(linkOfTheNews)
        webView = WKWebView(frame: self.view.frame)
        webView.load(URLRequest(url: URL(string: toShow)!))
        webView.navigationDelegate = self
        
        self.view.addSubview(webView)
        
    }


}


extension DetailNewsVC: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loadingActivityInd.stopAnimating()
    }
}
