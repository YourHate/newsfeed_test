//
//  ImageDownloader.swift
//
//
//  Created by Dmytro on 12/1/16.
//  Copyright © 2016 Dmytro Bondarenko. All rights reserved.
//

import UIKit

var imgDict = [String: UIImage]()

struct ImageDownloader {
    
    func downloadImages(_ photoLink: String, ifPhotoAvailable: ((UIImage)->())?, ifPhotoUnavailable: ((UIImage?)->())?){
        if let img = imgDict[photoLink] {
            ifPhotoAvailable!(img)
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            if let imageURL = URL(string: photoLink) {
                URLSession.shared.dataTask(with: imageURL) { (data, response, error) in
                    guard let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                        let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                        let data = data, error == nil,
                        let image = UIImage(data: data)
                        else { return }
                    
                        DispatchQueue.main.async {
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            imgDict[photoLink] = image
                            ifPhotoUnavailable!(image)
                        }
                    }.resume()
                    } else {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        print("I cant download this image :(")
                    }
            
            
        }
    }
    
  
    
}
