//
//  qqTableViewCell.swift
//  newsFeed
//
//  Created by Artem on 5/12/17.
//  Copyright © 2017 Artem Velykyy. All rights reserved.
//

import UIKit

class qqTableViewCell: UITableViewCell, RevMobAdsDelegate {
    
    @IBOutlet weak var bannerView: RevMobBannerView!

    var counter = 0
    var appID = ""
    var banner: RevMobBanner?
    
    override func awakeFromNib() {
        super.awakeFromNib()
            self.backgroundColor = UIColor.clear
            //            RevMobAds.startSession(withAppID: "54afd1ea498d3ca8092735f4",
//                                   withSuccessHandler: completionBlock, andFailHandler: ({ (error) -> Void in
//                                    print("UPS")
//                                   }))
        }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    func tableSetup(id: Int) {
        appID = Constant.bannersIds[id]
        print(appID)
        RevMobAds.startSession(withAppID: appID,
                               withSuccessHandler: self.showBannerWithCustomFrame, andFailHandler: ({ (error) -> Void in
                                print("UPS")
                               }))
    }
    
    
        func showBannerWithCustomFrame(){
            bannerView = RevMobAds.session().bannerView()
            let completionBlock: (RevMobBannerView!) -> Void = { bannerV in
                            let x = CGFloat(0)
                            let y = CGFloat(0)
                            let width = CGFloat(UIScreen.main.bounds.width)
                            let height = CGFloat(50)
                            self.bannerView!.frame = CGRect(x: x, y: y, width: width, height: height)
                            self.bannerView!.autoresizingMask = [.flexibleTopMargin, .flexibleWidth] //Optional Parameters to handle Rotation Events
                //self.bannerView!.frame = self.bannerView.frame
                 self.addSubview(self.bannerView!)
            }
            
            let clickHandler: (RevMobBannerView!) -> Void = {bView in
                NSLog("[RevMob Sample App] BannerView Clicked")
            }
            self.bannerView!.load(successHandler: completionBlock, andLoadFailHandler: ({ (error) -> Void in
                print("UPS")
            }), onClickHandler: clickHandler)
        }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
