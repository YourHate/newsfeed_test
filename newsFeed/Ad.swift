//
//  Ad.swift
//  newsFeed
//
//  Created by Artem on 5/16/17.
//  Copyright © 2017 Artem Velykyy. All rights reserved.
//

import UIKit

class Ad: NSObject, RevMobAdsDelegate {
    
    var backgroundView: RevMobBannerView!
    var fullscreen: RevMobFullscreen?
    var video: RevMobFullscreen?
    var rewardedVideo: RevMobFullscreen?
    var banner: RevMobBanner?
    var bannerWithDelegate: RevMobBanner?
    var bannerView: RevMobBannerView?
    var link: RevMobAdLink?
    var popUp: RevMobPopup?
    
    private var minAge: UInt
    private var adType: showAdv
    private var appID: String

    enum showAdv: String {
        case Banner, Video, RewardedVideo, FullScreenAd, Link, PopUP
    }
    
    init(appID: String,adType: showAdv, bannerView: RevMobBannerView, minAge: UInt) {
        self.appID = appID
        self.minAge = minAge
        self.adType = adType
        self.backgroundView = bannerView
    }
    
    func toggleAdForTable( revBannerView: RevMobBannerView) {
        //backgroundView = revBannerView
        startSession()
    }
    
    func revmobSessionDidStart() {
        RevMobAds.session().userAgeRangeMin = minAge
        switch adType {
        case .Banner:
            showBannerWithCustomFrame()
        case .FullScreenAd:
            showFullScreenWithDelegate()
        case .Video:
            showVideo()
        case .RewardedVideo:
            showRewardedVideo()
        case .Link:
            loadAdLink()
        case .PopUP:
            showPopUp()
        default:
            showVideo()
        }

    }

    func startSession() {
        RevMobAds.startSession(withAppID: appID, andDelegate: self)
    }

    
    
    // ADV setup
    //#MARK:- Fullscreen
    func showFullScreenWithDelegate(){
        //RevMobAds.session().showFullscreen()
        let fs = RevMobAds.session().fullscreen()
        fs?.delegate = self
        fs?.showAd()
    }
    //#MARK:- Video
    func showVideo(){
        video = RevMobAds.session().fullscreen()
        video!.delegate = self
        video!.loadVideo()    
    }
    func revmobVideoDidLoad(_ placementId: String!) {
        print("RevMob Video loaded")
        video!.showVideo()
    }
    //#MARK:- Rewarded Video
    func showRewardedVideo(){
        rewardedVideo = RevMobAds.session().fullscreen()
        rewardedVideo!.delegate = self
        rewardedVideo!.loadRewardedVideo()
        
    }
    func revmobRewardedVideoDidLoad(_ placementId: String!) {
        print("RevMob Rewarded Video  loaded")
        rewardedVideo!.showRewardedVideo()
    }
    //Native button
    func loadAdLink(){
        RevMobAds.session().openLink()
    }
    func revmobNativeDidReceive(_ placementId: String!) {
        print("RevMob  Native Link received")
    }

    //#MARK:- PopUp
    func showPopUp(){
        RevMobAds.session().showPopup()
        //popUp = RevMobAds.session().popup()
        //let completionBlock: (RevMobPopup!) -> Void = {popup in
        //    print("RevMob PopUp loaded")
        //    self.popUp!.showAd()
        //               print("RevMob PopUp displayed")
        //}
        //let failureBlock: (RevMobPopup?, NSError?) -> Void = {popup,error in
        //    print("RevMob PopUp failed to load with error" , error)
        //}
        //let clickHandler: (RevMobPopup! ) -> Void = { popup in
        //    print("RevMob PopUp clicked")
        //}
        //popUp!.load(successHandler: completionBlock, andLoadFailHandler: failureBlock as! RevMobPopupFailureHandler, onClickHandler: clickHandler)
        
    }
    func revmobPopUpDidReceive(_ placementId: String!) {
        print("RevMob PopUp received")
    }

    //#MARK:- Banner
    func showBannerWithDelegate(){
        bannerWithDelegate?.showAd()
    }
    
    func showBannerWithCustomFrame() {
        bannerView = RevMobAds.session().bannerView()
        let completionBlock: (RevMobBannerView!) -> Void = { bannerV in
            let x = self.backgroundView.bounds.origin.x
            let y = self.backgroundView.bounds.origin.y
            let width = self.backgroundView.bounds.width
            let height = self.backgroundView.bounds.height
            self.bannerView!.frame = CGRect(x: x, y: y, width: width, height: height)
            self.bannerView!.autoresizingMask = [.flexibleTopMargin, .flexibleWidth] //Optional Parameters to handle Rotation Events
            //self.bannerView!.frame = self.bannerView.frame
            self.backgroundView.addSubview(self.bannerView!)
        }

        let clickHandler: (RevMobBannerView!) -> Void = {bView in
            NSLog("[RevMob Sample App] BannerView Clicked")
        }
        self.bannerView!.load(successHandler: completionBlock, andLoadFailHandler: ({ (error) -> Void in
            print("UPS")
        }), onClickHandler: clickHandler)
    }
}


